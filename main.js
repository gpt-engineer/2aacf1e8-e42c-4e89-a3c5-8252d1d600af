const dino = document.querySelector('.dino');
const obstacle = document.querySelector('.obstacle');
const scoreDisplay = document.querySelector('#score');
let score = 0;

function jump() {
    // Add jump logic here
}

function duck() {
    // Add duck logic here
}

function moveObstacle() {
    // Add obstacle movement logic here
}

function checkCollision() {
    // Add collision detection logic here
}

function increaseScore() {
    score++;
    scoreDisplay.textContent = score;
}

// Add event listeners for jump and duck actions
window.addEventListener('keyup', function(event) {
    if (event.code === 'Space') {
        jump();
    } else if (event.code === 'ArrowDown') {
        duck();
    }
});

// Start the game
moveObstacle();
